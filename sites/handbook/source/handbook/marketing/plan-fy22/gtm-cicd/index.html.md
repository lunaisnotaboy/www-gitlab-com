---
layout: handbook-page-toc
title: "CI/CD GTM Sales Plays"
description: "description to add"
twitter_image: '/images/tweets/handbook-marketing.png'
---

## CI/CD GTM Motion 

### Integrated campaigns
1. Upsell Premium => Ultimate campaign
   - Campaign brief (placeholder)

### Sales plays

1. [Sales Playbook for Upsell Premium => Ultimate](/handbook/marketing/plan-fy22/gtm-cicd/premium-to-ultimate-planner/fy22-gtm-premium-to-ultimate-playbook)
   - [Message house](/handbook/marketing/plan-fy22/gtm-cicd/premium-to-ultimate-planner/fy22-gtm-premium-to-ultimate-playbook/message-house)
   - [Strategy/planner doc](/handbook/marketing/plan-fy22/gtm-cicd/premium-to-ultimate-planner) for visibility

Additional sales plays will be added as they're available

#### Other resources
